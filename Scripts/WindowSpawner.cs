using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WindowsSolitair;
using WindowsSolitair.Scripts;

public partial class WindowSpawner : Node
{
	[Export] private Sprite2D _deckTopPosition;
	[Export] private Button _dealButton;
	[Export] private Button _packButton;
	[Export] private Label _deckSizeLabel;
	//private readonly List<CardStackWindow> _windows = new();
	private readonly List<CardWindow> _allWindows = new();
	private readonly List<CardStackWindow> _tableauWindows = new();
	private readonly List<CardStackWindow> _foundationWindows = new();
	private CardStackWindow _stockPile;

	[Export] private double _spawnCooldown = 1;
	private List<Card> _deck;
	private CardWindow _movingCards;
	private CardStackWindow _movingCardsStack;

	public override void _Input(InputEvent @event)
	{
		if(@event is InputEventMouseButton {ButtonIndex: MouseButton.Left, Pressed: true} eventMouseButton &&
		   eventMouseButton.Position.Y >= _deckTopPosition.GlobalPosition.Y && eventMouseButton.Position.Y <= _deckTopPosition.GlobalPosition.Y + Deck.CardWidth && 
		   eventMouseButton.Position.X >= _deckTopPosition.GlobalPosition.X && eventMouseButton.Position.X <= _deckTopPosition.GlobalPosition.X + Deck.CardHeight)
			DealStockPile();
			
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_dealButton.Pressed += DealTableaus;
		_packButton.Pressed += () => PackUp();
		_packButton.Disabled = true;
		_deck = Deck.GetFullDeckShuffled();
		
	}

	public override void _Process(double delta)
	{
		_deckSizeLabel.Text = _deck.Count.ToString();
	}

	private async void DealStockPile()
	{
		if (IsInstanceValid(_stockPile))
		{
			if(IsInstanceValid(_movingCards) && _movingCardsStack == _stockPile)
				_deck.Add(_movingCards.RemoveCard(0));
			_deck.AddRange(_stockPile.GetAllCards());
			_stockPile.Free();
		}
		if(_deck.Count == 0)
			return;
		_deck[0].IsFlipped = true;
		_stockPile = SpawnStack();
		_stockPile.AddCard(_deck[0]);
		_deck.RemoveAt(0);
		_stockPile.IsHorizontal = true;
		_stockPile.StockPick = true;
		_stockPile.HiddenStackOffset = _stockPile.VisibleStackOffset;
		_stockPile.MoveTweened(GetWindow().Position + (100 + GetWindow().Size.X) * Vector2I.Right);

		CardWindow w = null;
		if (_deck.Count > 0)
		{
			await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
			_deck[0].IsFlipped = true;
			w = SpawnSingle(_deck[0]);
			w.TargetStack = _stockPile;
			w.IsSystemStack = true;
			_deck.RemoveAt(0);
			w.MoveTweened(GetWindow().Position + (100 + GetWindow().Size.X) * Vector2I.Right);
		}		
		if (_deck.Count > 0)
		{
			await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
			_deck[0].IsFlipped = true;
			w = SpawnSingle(_deck[0]);
			w.TargetStack = _stockPile;
			w.IsSystemStack = true;
			_deck.RemoveAt(0);
			w.MoveTweened(GetWindow().Position + (100 + GetWindow().Size.X) * Vector2I.Right);
		}
		if(IsInstanceValid(w))
			await ToSignal(w, Node.SignalName.TreeExited);

		_stockPile.FlipAllCards();
		
	}

	public CardStackWindow SpawnStack()
	{
		CardStackWindow w = new();
		w.InitialPosition = Window.WindowInitialPosition.Absolute;
		w.Position = GetWindow().Position + (Vector2I)_deckTopPosition.GlobalPosition;
		w.TreeExited += () => { _allWindows.Remove(w); };
		AddChild(w);
		_allWindows.Add(w);
		

		return w;
	}

	public CardWindow SpawnSingle(Card card)
	{
		CardWindow w = new();
		w.AddCard(card);
		w.InitialPosition = Window.WindowInitialPosition.Absolute;
		w.Position = GetWindow().Position + (Vector2I)_deckTopPosition.GlobalPosition;
		w.WindowMoved += () => { CheckForCollisions(w); };
		w.TreeExited += () => { _allWindows.Remove(w); };
		AddChild(w);
		_allWindows.Add(w);

		return w;
	}
	
	public CardStackWindow SpawnFoundation()
	{
		CardStackWindow w = new();
		w.InitialPosition = Window.WindowInitialPosition.Absolute;
		w.Position = GetWindow().Position + (Vector2I)_deckTopPosition.GlobalPosition;
		w.TreeExited += () => 
		{ 
			_allWindows.Remove(w);
			_foundationWindows.Remove(w);
		};
		w.CardAdded += CheckWinCondition;
		w.HiddenStackOffset = 0;
		w.VisibleStackOffset = 0;
		w.IsLocked = true;
		w.IsFoundation = true;
		AddChild(w);
		_allWindows.Add(w);
		_foundationWindows.Add(w);

		return w;
	}

	public CardWindow SpawnTableauSingle(Card card, CardStackWindow tableau)
	{
		if (IsInstanceValid(_movingCards) && IsInstanceValid(_movingCardsStack))
		{
			_movingCards.TargetStack = _movingCardsStack;
			_movingCards.IsSystemStack = true;
			_movingCards.MoveTweened(_movingCardsStack.Position + _movingCardsStack.GetNextCardPosition());
			_movingCardsStack.IsLocked = false;
		}

		if (card.Value == CardValue.Ace)
		{
			CardStackWindow aceW = SpawnFoundation();
			aceW.AddCard(card);
			card.IsFlipped = false;
			tableau.RevealTopCard();
			return aceW;

		}
		CardWindow w = SpawnSingle(card);
		w.TargetStack = tableau;
		_movingCards = w;
		_movingCards.TreeExited += () =>
		{
			tableau.RevealTopCard();
			tableau.IsLocked = false;
		};
		_movingCardsStack = tableau;
		_movingCardsStack.IsLocked = true;
		return w;

	}


	private async void DealTableaus()
	{

		_dealButton.Disabled = true;
		Vector2I[] tableauSpots = new Vector2I[7];
		int screenVerticalCenter = DisplayServer.ScreenGetPosition().Y + (DisplayServer.ScreenGetSize().Y / 2);
		int mainWindowVerticalCenter = GetWindow().Position.Y + (GetWindow().Size.Y / 2);
		int tableauPositionY;
		if (mainWindowVerticalCenter <= screenVerticalCenter)
			tableauPositionY = screenVerticalCenter + DisplayServer.ScreenGetSize().Y / 4;
		else
			tableauPositionY = screenVerticalCenter - DisplayServer.ScreenGetSize().Y / 4;

		int tableauPositionX = DisplayServer.ScreenGetPosition().X;
		int tableauOffsetX = DisplayServer.ScreenGetSize().X / 9;

		for (int i = 0; i < tableauSpots.Length; i++)
		{
			tableauSpots[i] = new(tableauPositionX + (i + 1) * tableauOffsetX, tableauPositionY);
		}

		CardWindow lastWindow = null;
		for (int i = 0; i < tableauSpots.Length; i++)
			if (_deck.Count > 0)
			{
				await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
				CardStackWindow w = SpawnStack();
				_deck[0].IsFlipped = true;
				w.AddCard(_deck[0]);
				_tableauWindows.Add(w);
				w.TreeExited += () => _tableauWindows.Remove(w);
				lastWindow = w;
				_deck.RemoveAt(0);
				w.MoveTweened(tableauSpots[i]);
			}
		for (int i = 1; i < tableauSpots.Length; i++)
		for (int j = i; j < tableauSpots.Length; j++)
			if (_deck.Count > 0)
			{
				await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
				_deck[0].IsFlipped = true;
				CardWindow w = SpawnSingle(_deck[0]);
				w.TargetStack = _tableauWindows[j];
				w.IsSystemStack = true;
				lastWindow = w;
				_deck.RemoveAt(0);
				w.MoveTweened(_tableauWindows[j].Position + _tableauWindows[j].GetNextCardPosition());
			}

		if (lastWindow != null)
			await ToSignal(lastWindow, Node.SignalName.TreeExited);
		//_spawnQueue.Enqueue(new Tuple<Card, Vector2I>(, tableauSpots[j]));
		//SpawnStack().MoveTweened(tableauSpots[j]);

		foreach (CardStackWindow window in _tableauWindows)
		{
			window.FlipTopCard();
			await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);

		}
		
		_packButton.Disabled = false;
	}

	private async Task PackUp()
	{
		if(_allWindows.Count == 0)
			return;
		
		_packButton.Disabled = true;
		CardWindow lastSingle = null;
		while(_allWindows.Count > 0)
		{
			CardWindow currentWindow = _allWindows[0];
			while (currentWindow.NumCards > 0)
			{
				Vector2I topCardPosition = currentWindow.Position + (Vector2I)currentWindow.TopCard.Position;
				CardWindow single = SpawnSingle(currentWindow.RemoveCard(currentWindow.NumCards - 1));
				_allWindows.Remove(single);
				single.Position = topCardPosition;
				single.IsSystemStack = true;
				single.TweenFinished += () =>
				{
					single.QueueFree();
				};
				single.MoveTweened(GetWindow().Position + (Vector2I)_deckTopPosition.GlobalPosition);
				lastSingle = single;
				
				await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
			}
			currentWindow.Free();
		}

		await ToSignal(lastSingle, CardWindow.SignalName.TweenFinished);
		Reset();
		
		_dealButton.Disabled = false;
	}

	private void Reset()
	{
		while (_allWindows.Count > 0)
		{
			if(IsInstanceValid(_allWindows[0]))
				_allWindows[0].QueueFree();
			_allWindows.RemoveAt(0);
		}
		_deck = Deck.GetFullDeckShuffled();
	}


	private void CheckForCollisions(CardWindow indexWindow)
	{
		for (int i = 0; i < _tableauWindows.Count; i++)
		{
			CardStackWindow collisionWindow = _tableauWindows[i];

			if (DoIntersect(indexWindow,collisionWindow))
			{
				if(indexWindow.IsSystemStack)
				{
					if (collisionWindow == indexWindow.TargetStack)
					{
							indexWindow.GetAllCards().ForEach((card => collisionWindow.AddCard(card)));
							return;
					}
						continue;
				}

				if (collisionWindow == indexWindow.TargetStack || collisionWindow.AcceptsCard(indexWindow.BottomCard))
				{
					indexWindow.GetAllCards().ForEach((card => collisionWindow.AddCard(card)));
					return;
				}
				
			}
		}
		for (int i = 0; i < _foundationWindows.Count; i++)
		{
			CardStackWindow collisionWindow = _foundationWindows[i];

			if (DoIntersect(indexWindow,collisionWindow))
			{
				if(indexWindow.IsSystemStack)
				{
					if (collisionWindow == indexWindow.TargetStack)
					{
						indexWindow.GetAllCards().ForEach((card => collisionWindow.AddCard(card)));
						return;
					}
					continue;
				}

				if (collisionWindow == indexWindow.TargetStack || collisionWindow.AcceptsCard(indexWindow.BottomCard))
				{
					indexWindow.GetAllCards().ForEach((card => collisionWindow.AddCard(card)));
					return;
				}
				
			}
		}
		
		if (IsInstanceValid(_stockPile) && DoIntersect(indexWindow,_stockPile))
		{
			if(indexWindow.IsSystemStack)
			{
				if(_stockPile == indexWindow.TargetStack)
					indexWindow.GetAllCards().ForEach((card => _stockPile.AddCard(card)));
			}
			if (_stockPile == indexWindow.TargetStack || _stockPile.AcceptsCard(indexWindow.BottomCard))
				indexWindow.GetAllCards().ForEach((card => _stockPile.AddCard(card)));
				
		}
	}

	public void CheckWinCondition()
	{
		foreach (CardStackWindow window in _foundationWindows)
		{
			if(window.GetChildCount() < 13)
				return;
		}
		PlayVictoryAnimation();
		
		Reset();
	}

	public async Task VictoryAnimationDebugSetup()
	{
		//await PackUp();
		List<Card> deck = Deck.GetFullDeck();
		foreach (Card ace in deck.FindAll(card => card.Value == CardValue.Ace))
		{
			CardStackWindow aceWindow = SpawnFoundation();
			ace.IsFlipped = false;
			aceWindow.AddCard(ace);
			foreach (Card nonAce in deck.FindAll(card => card.Suit == ace.Suit && card.Value != CardValue.Ace))
			{
				nonAce.IsFlipped = false;
				aceWindow.AddCard(nonAce);
			}
			RandomNumberGenerator rnd = new();
			aceWindow.Position += new Vector2I(rnd.RandiRange(-500, 500), rnd.RandiRange(-500, 500));
			await ToSignal(GetTree().CreateTimer(_spawnCooldown), Timer.SignalName.Timeout);
		}
	}

	public void PlayVictoryAnimation()
	{
		//await VictoryAnimationDebugSetup();
		VictoryAnimationWindow vaw = new ();
		vaw.Setup();
		AddChild(vaw);
		foreach (CardStackWindow foundationWindow in _foundationWindows)
		{
			vaw.SetupStack(foundationWindow.GetAllCards(), foundationWindow.Position);
		}
		
		vaw.StartAnimation();
		_dealButton.Disabled = false;
		_packButton.Disabled = true;
	}

	public bool DoIntersect(Window a, Window b)
	{
		int widthOfLefter = a.Position.X < b.Position.X ? a.Size.X : b.Size.X;
		int heightOfUpper = a.Position.Y < b.Position.Y ? a.Size.Y : b.Size.Y;
		return Math.Abs(a.Position.X - b.Position.X) < widthOfLefter &&
		       Math.Abs(a.Position.Y - b.Position.Y) < heightOfUpper;
	}

	public override void _Notification(int what)
	{
		switch (what)
		{
			case (int)MainLoop.NotificationApplicationFocusIn:
				//foreach (CardStackWindow window in _tableauWindows)
				{
					
					//window.Show();
					//window.Reappear();
				}

				break;
			case (int)MainLoop.NotificationApplicationFocusOut:
				//foreach (CardStackWindow window in _tableauWindows)
				{
					//window.Hide();
					//window.Disappear();
				}

				break;
		}
	}
}