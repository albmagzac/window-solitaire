using Godot;
using System;
using WindowsSolitair;

public partial class Card : Node2D
{
	public CardSuit Suit;
	public CardValue Value;
	
	public Rect2 FrontFaceRect;
	
	private bool _isFlipped;

	[Export] private Sprite2D _visual;
	[Export] private AnimationPlayer _animationPlayer;

	public bool IsFlipped
	{
		get => _isFlipped;
		set
		{
			_isFlipped = value;
			_visual.RegionRect = _isFlipped ? Deck.GetBackSideRect() : FrontFaceRect;
		}
	}

	public override void _Ready()
	{
		_visual.Centered = false;
		_visual.RegionEnabled = true;
		TextureFilter = TextureFilterEnum.Nearest;
	}

	private void Flip()
	{
		IsFlipped = !IsFlipped;
	}

	public void FlipCard()
	{
		_animationPlayer.Play("flip");
	}

	public void Disappear()
	{
		_animationPlayer.Play("disappear");
	}

	public void Reappear()
	{
		_animationPlayer.PlayBackwards("disappear");
	}

}
