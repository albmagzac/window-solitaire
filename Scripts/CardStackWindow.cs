﻿using System.Collections.Generic;
using Godot;
using WindowsSolitair.Scripts;

namespace WindowsSolitair;

public partial class CardStackWindow : CardWindow
{
	
	private Card _emptyStackVisual;
	
	public bool StockPick = false;
	public bool IsFoundation = false;

	public bool IsLocked = false;

	public CardStackWindow()
	{
		_emptyStackVisual = Deck.GetEmptyStack();
		AddChild(_emptyStackVisual);
	}

	public override void _Input(InputEvent @event)
	{
		if(NumCards == 0)
			return;
		
		if (@event is InputEventMouseButton { ButtonIndex: MouseButton.Left, Pressed: true } leftMouseButtonEvent && !TopCard.IsFlipped && !IsLocked)
		{
			int clickedCardNumber = 1;

			for (int i = 1; i < NumCards; i++)
			{
				Card card = GetChild<Card>(-i);
				if(card.IsFlipped)
					return;
				if(IsHorizontal ? leftMouseButtonEvent.Position.X <  card.Position.X : 
					   leftMouseButtonEvent.Position.Y <  card.Position.Y)
					clickedCardNumber++;
				else
					break;	
			}



			if (StockPick)
				clickedCardNumber = 1;

			List<Card> newCardStack = RemoveTopCards(clickedCardNumber);
			CardWindow w = GetParent<WindowSpawner>().SpawnTableauSingle(newCardStack[0], this);
			w.Position = Position + GetNextCardPosition();
			for(int i = 1; i < newCardStack.Count; i++)
			    w.AddCard(newCardStack[i]);
			w._Input(@event);
			return;
		}
		base._Input(@event);

	}



	private List<Card> RemoveTopCards(int amount)
	{
		List<Card> cards = new();
		for (int i = 0; i < amount; i++)
		{
			cards.Insert(0,RemoveCard(-1));
		}

		return cards;
	}



	public void FlipTopCard()
	{
		Card topCard = GetChild<Card>(GetChildCount() - 1);
		topCard.FlipCard();
	}

	public void RevealTopCard()
	{
		if(NumCards > 0 && TopCard.IsFlipped)
			FlipTopCard();
	}

	public async void FlipAllCards()
	{
		for (int i = 1; i <= NumCards; i++)
		{
			GetChild<Card>(-i).FlipCard();
			await ToSignal(GetTree().CreateTimer(0.3), Timer.SignalName.Timeout);
		}
	}

	public void Disappear()
	{
		foreach (Node node in GetChildren())
		{
			Card card = (Card)node;
			card.Disappear();
		}
	}

	public void Reappear()
	{
		foreach (Node node in GetChildren())
		{
			Card card = (Card)node;
			card.Reappear();
		}
	}

	public bool AcceptsCard(Card card)
	{
		if (IsFoundation)
			return card.Suit == TopCard.Suit && card.Value == TopCard.Value + 1;
		return !StockPick && 
			(card.IsFlipped || 
			(NumCards == 0 && card.Value == CardValue.King) || 
			(NumCards > 0 && ((int)card.Suit)%2 != ((int)TopCard.Suit)%2 && card.Value == TopCard.Value - 1));
	}

	public override void AddCard(Card card)
	{
		if (_isEmpty)
		{
			RemoveChild(_emptyStackVisual);
		}
		base.AddCard(card);
	}

	public override Card RemoveCard(int index)
	{
		if (NumCards > index)
		{
			Card c = GetChild<Card>(index);
			RemoveChild(c);

			if (NumCards == 0)
			{
				_isEmpty = true;
				AddChild(_emptyStackVisual);
			}
			UpdateVisual();
			
			return c;
		}

		return null;
	}
}