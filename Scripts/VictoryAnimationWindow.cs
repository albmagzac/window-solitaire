﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Godot;

namespace WindowsSolitair.Scripts;

public partial class VictoryAnimationWindow : Window
{
	private SubViewport _subViewport;
	public VictoryAnimationWindow()
	{
		


	}

	public override void _Input(InputEvent @event)
	{
		if(@event is InputEventMouseButton {ButtonIndex: MouseButton.Left, Pressed:true} mouseButtonEvent)
			QueueFree();
	}

	public void Setup()
	{
		int numScreens = DisplayServer.GetScreenCount();
		int maxX = 0, maxY = 0;
		for (int i = 0; i < numScreens; i++)
		{
			Vector2I screenPos = DisplayServer.ScreenGetPosition(i) + DisplayServer.ScreenGetSize(i);
			if (screenPos.X > maxX)
				maxX = screenPos.X;
			if (screenPos.Y > maxY)
				maxY = screenPos.Y;
		}

		InitialPosition = WindowInitialPosition.Absolute;
		Size = new Vector2I(maxX, maxY);
		//Size = new Vector2I(300, 300);
		Position = new Vector2I(-1, 0);
		Borderless = true;
		
		// Passthrough area not visible on windows
		//MousePassthrough = true;
		//MousePassthroughPolygon = new[] { new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1) };


		Transparent = true;
		AlwaysOnTop = true;
		SubViewportContainer svc = new();
		svc.Position = Vector2.Zero;
		svc.Size = Size;
		AddChild(svc);
		_subViewport = new ();
		_subViewport.Size = Size;
		_subViewport.TransparentBg = true;
		_subViewport.RenderTargetClearMode = SubViewport.ClearMode.Never;
		svc.AddChild(_subViewport);
		ColorRect crr = new();
		crr.Size = new Vector2(100, 100);
		crr.Position = new Vector2(200, 200);
		crr.Color = Colors.Red;
		ColorRect crb = new ();
		crb.Position = Size - new Vector2I(500, 500);
		crb.Color = Colors.Blue;
		crb.Size = crr.Size;
		//_subViewport.AddChild(crr);
		//_subViewport.AddChild(crb);
		
	}

	public async void StartAnimation()
	{
		float colliderThickness = 100f;
		StaticBody2D floor = new();
		CollisionShape2D floorCollider = new() { Shape = new RectangleShape2D { Size = new Vector2(_subViewport.Size.X, colliderThickness)}};
		floor.Position = new(_subViewport.Size.X / 2f, _subViewport.Size.Y + colliderThickness / 2);
		//floor.Position = new(_subViewport.Size.X / 2f, 1000);
		floor.AddChild(floorCollider);
		floor.CollisionLayer = 2;
		_subViewport.AddChild(floor);

		//Wait a single frame to ensure the top cards get drawn into the viewport
		await ToSignal(GetTree(), SceneTree.SignalName.ProcessFrame);
		
		List<BouncyBody> bodies = new();
		for (int i = _subViewport.GetChildCount() - 1; i >= 0; i--)
		{
			if (_subViewport.GetChild(i) is BouncyBody body)
			{
				body.Visible = false;
				bodies.Add(body);
			}
		}

		for (int i = 0; i < bodies.Count; i++)
		{
			RandomNumberGenerator rng = new();
			bodies[i].Visible = true;
			bodies[i].Velocity = new Vector2(rng.RandfRange(-800, 800), rng.RandfRange(-800, -400));
			bodies[i].IsLocked = false;
			_subViewport.MoveChild(bodies[i], -1);

			await ToSignal(GetTree().CreateTimer(1), Timer.SignalName.Timeout);
		}
	}

	public void SetupStack(List<Card> stack, Vector2I globalPosition)
	{
		List<BouncyBody> bodies = new();
		foreach (Card card in stack)
		{
			BouncyBody bb = new ();
			CollisionShape2D cs2 = new ();
			cs2.Shape = new RectangleShape2D { Size = new Vector2(Deck.CardWidth, Deck.CardHeight) };
			cs2.Position = new Vector2(Deck.CardWidth / 2f, Deck.CardHeight / 2f);
			bb.AddChild(cs2);
			card.Position = Vector2.Zero;
			bb.AddChild(card);
			bb.CollisionLayer = 1;
			bb.CollisionMask = 2;
			bb.Position = globalPosition;
			bb.Visible = card.Value == CardValue.King;
			bb.Name = card.Name;
			bodies.Add(bb);
		}
		
		int interlacePosition = 0;
		int sizeRelation = (stack.Count + _subViewport.GetChildCount()) / stack.Count;
		RandomNumberGenerator rng = new();
		while (bodies.Count > 0)
		{
			int random = rng.RandiRange(1, sizeRelation);
			if (random == 1)
			{
				_subViewport.AddChild(bodies[0]);
				_subViewport.MoveChild(bodies[0], interlacePosition);
				bodies.RemoveAt(0);
			}
			
			interlacePosition = Math.Clamp(interlacePosition+1,0, _subViewport.GetChildCount());
		}
	}
}