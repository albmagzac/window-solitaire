﻿using System.Collections.Generic;
using Godot;

namespace WindowsSolitair.Scripts;

public partial class CardWindow : Window
{
	private Vector2 _realPosition;
	private Vector2 _mouseOldSpotRelative;

	private bool _pressed;

	public CardStackWindow TargetStack;
	public bool IsSystemStack;
	public CardWindow()
	{
		Borderless = true;
		Transparent = true;
		AlwaysOnTop = true;
		
		Size = new Vector2I(Deck.CardWidth, Deck.CardHeight);
	}
	public override void _Ready()
	{
		
	}

	public override void _Input(InputEvent @event)
	{
		if(IsSystemStack)
			return;
		
		if (@event is InputEventMouseButton leftMouseButtonEvent)
		{
			Input.MouseMode = leftMouseButtonEvent.Pressed ? Input.MouseModeEnum.Captured : Input.MouseModeEnum.Visible;
			
			if (_pressed != leftMouseButtonEvent.Pressed)
			{
				// Mouse was just pressed
				if (leftMouseButtonEvent.Pressed)
				{
					_mouseOldSpotRelative =
						leftMouseButtonEvent.GlobalPosition;
					_realPosition = GetWindow().Position;
				}
				// Mouse was just released
				else
				{
					Input.WarpMouse(_mouseOldSpotRelative);
					EmitSignal(SignalName.WindowMoved);
				}
				
				
			}
			
			_pressed = leftMouseButtonEvent.Pressed;
			
		}
		if (@event is InputEventMouseMotion mouseMoveEvent && _pressed)
		{
			_realPosition += mouseMoveEvent.Relative;
			Position = (Vector2I)_realPosition;

		}
	}


	public void MoveTweened(Vector2I newPosition)
	{
		Tween tween = GetTree().CreateTween();
		tween.TweenProperty(this, "position", newPosition, .5d)
			.SetTrans(Tween.TransitionType.Circ);
		tween.TweenCallback(Callable.From(()=>
		{
			EmitSignal(SignalName.WindowMoved);
			EmitSignal(SignalName.TweenFinished);
		}));
	}
	
	[Signal]
	public delegate void WindowMovedEventHandler();

	[Signal]
	public delegate void TweenFinishedEventHandler();
	
	public bool IsHorizontal = false;
	public int HiddenStackOffset = 6;
	public int VisibleStackOffset = 40;
	
	
	public Card TopCard => _isEmpty ? null : GetChild<Card>(-1);
	public Card BottomCard => _isEmpty ? null : GetChild<Card>(0);
	public int NumCards => _isEmpty ? 0 : GetChildCount();

	protected bool _isEmpty = true;

	public virtual void AddCard(Card card)
	{
		
		_isEmpty = false;
		if(card.GetParent() != null)
			card.Reparent(this);
		else
			AddChild(card);
		
		UpdateVisual();
		EmitSignal(SignalName.CardAdded);
	}

	public virtual Card RemoveCard(int index)
	{
		if (NumCards > index)
		{
			Card c = GetChild<Card>(index);
			RemoveChild(c);
			UpdateVisual();

			_isEmpty = NumCards <= 0;

			
			return c;
		}

		return null;
	}
	
	protected void UpdateVisual()
	{
		if(GetChildCount() == 0)
			return;
		GetChild<Card>(0).Position = Vector2.Zero;
		for (int i = 1; i < GetChildCount(); i++)
		{

			GetChild<Card>(i).Position = GetNextCardPosition(GetChild<Card>(i - 1));

		}

		Size = GetNextCardPosition() + new Vector2I(Deck.CardWidth, Deck.CardHeight);
	}
	
	public Vector2I GetNextCardPosition()
	{
		if (NumCards == 0)
			return Vector2I.Zero;
		return (Vector2I)TopCard.Position +
		       (IsHorizontal ? Vector2I.Right : Vector2I.Down) * (TopCard.IsFlipped ? HiddenStackOffset : VisibleStackOffset);
	}
	public Vector2I GetNextCardPosition(Card card)
	{
		if (NumCards == 0)
			return Vector2I.Zero;
		return (Vector2I)card.Position +
		       (IsHorizontal ? Vector2I.Right : Vector2I.Down) * (card.IsFlipped ? HiddenStackOffset : VisibleStackOffset);
	}
	
	public List<Card> GetAllCards()
	{
		List<Card> cards = new();
		while(NumCards > 0)
		{
			Card card = GetChild<Card>(0);
			cards.Add(card);
			RemoveChild(card);
		}
		QueueFree();
		return cards;
	}

	[Signal]
	public delegate void CardAddedEventHandler();
}