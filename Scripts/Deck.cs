﻿using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Godot.NativeInterop;

namespace WindowsSolitair;

public enum CardSuit { Spade, Heart, Club, Diamond}
public enum CardValue {Ace = 0, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King}

public partial class Deck : Node
{
	private static Deck _instance;
	
	private const int _CardWidth = 71;
	private const int _CardHeight = 96;

	public static int Scale = 2;
	public static int CardWidth => _CardWidth * Scale;
	public static int CardHeight => _CardHeight * Scale;


	[Export] private PackedScene _cardPrefab; 

	public override void _Ready()
	{
		if (_instance == null)
			_instance = this;
		else
			QueueFree();	
	}

	private Card PGetCard(int xSheetPos, int ySheetPos)
	{
		Card card =  _cardPrefab.Instantiate<Card>();

		card.FrontFaceRect = new(xSheetPos * _CardWidth, ySheetPos * _CardHeight, _CardWidth, _CardHeight);
		card.Scale = new(Scale, Scale);

		return card;
	}
	
	public static Card GetCard(CardSuit suit, CardValue value)
	{
		Card card = _instance.PGetCard((int)value,(int)suit);
		card.Name = $"{suit.ToString()}_{value.ToString()}";
		card.Suit = suit;
		card.Value = value;
		return card;
	}
	

	public static Card GetEmptyStack()
	{
		Card emptyStack = _instance.PGetCard(12,4);
		emptyStack.Name = "Empty_Stack";
		emptyStack.IsFlipped = false;
		return emptyStack;
	}

	public static List<Card> GetFullDeck()
	{
		List<Card> deck = new();
		for (int s = 0; s < Enum.GetNames(typeof(CardSuit)).Length; s++)
		{
			for (int v = 0; v < Enum.GetNames(typeof(CardValue)).Length; v++)
			{
				deck.Add(GetCard((CardSuit)s,(CardValue)v));
			}

		}
		return deck;
	}

	public static List<Card> GetFullDeckShuffled()
	{
		List<Card> deck = GetFullDeck();
		RandomNumberGenerator rng = new();
		int n = deck.Count;  
		while (n > 1) {  
				n--;  
				int k = rng.RandiRange(0,n);  
				(deck[k], deck[n]) = (deck[n], deck[k]);
		}

		return deck;

	}

	public static Rect2 GetBackSideRect()
	{
		return new(71.0f, 480.0f, 71.0f, 96.0f);
	}
	
	public static void ShuffleDeck(ref Card[] cards) =>
		cards = cards.OrderBy(x => Random.Shared.Next()).ToArray();
	
}