﻿using Godot;

namespace WindowsSolitair.Scripts;

public partial class BouncyBody : CharacterBody2D
{
	public bool IsLocked = true;
	public override void _Ready()
	{
		
	}

	public override void _PhysicsProcess(double delta)
	{
		if(IsLocked)
			return;
		KinematicCollision2D collision = MoveAndCollide(Velocity * (float)delta);
		if (collision != null)
		{
			if(Velocity.Y < 300f)
				QueueFree();
			Velocity = new Vector2(Velocity.X, Velocity.Y * -0.9f);
		}
		Velocity = new Vector2(Velocity.X, Velocity.Y + 2000f * (float)delta);
		
	}
}